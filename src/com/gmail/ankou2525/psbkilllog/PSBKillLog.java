package com.gmail.ankou2525.psbkilllog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class PSBKillLog extends JavaPlugin implements Listener{	
	public Economy economy = null;
	private Economy econ;
	List<Player> allow = new ArrayList<Player>();
	HashMap<Player,Integer> sd = new HashMap<Player,Integer>();
	String pf = ChatColor.DARK_GREEN+"["+ ChatColor.RESET +"お金"+ ChatColor.DARK_GREEN +"] ";
	public static Inventory inv = Bukkit.createInventory(null, 27, ChatColor.BOLD +"ゴミ箱");

	Logger log;

	public void onEnable(){
		log = this.getLogger();
		log.info("AnkouMix有効になりました。");
		this.setupEconomy();
		this.econ = this.economy;
		this.saveDefaultConfig();
		for(Player tg: Bukkit.getOnlinePlayers()){
			sd.put(tg, 0);
			allow.add(tg);
		}
		getServer().getPluginManager().registerEvents(this, this);
	}
	public void onDisable(){	
		log.info("AnkouMix無効になりました。");
	}
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		sd.put(p, 0);
	}
	//Economyをセットアップ。
	private boolean setupEconomy()
	{
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}

		return (economy != null);
	}
	@EventHandler
	//看板の文字が変更された時発生。
	public void onSCH(SignChangeEvent e){
		Player p = e.getPlayer();
		Sign si = (Sign) e.getBlock().getState();
		//看板の一番上の行に"keizi"と入力された場合。
		if(e.getLine(0).contains("keizi")){
			//1行目を"[掲示板]"に変更。
			e.setLine(0,ChatColor.AQUA+"[掲示板]");
			Bukkit.broadcastMessage(p.getName() + ChatColor.LIGHT_PURPLE +"が掲示板を設置。");
			//2行目以降の文字を取得しメッセージ送信。
			Bukkit.broadcastMessage("2行目: "+ e.getLine(1));
			Bukkit.broadcastMessage("3行目: "+ e.getLine(2));
			Bukkit.broadcastMessage("4行目: "+ e.getLine(3));
			//看板の情報を更新。(無くてもいける？)
			si.update();
		}
	}
	@EventHandler
	//インベントリが閉じられた時に発生。
	public void onInventoryClose(InventoryCloseEvent e){
		Player player = (Player) e.getPlayer();
		//閉じたインベントリの名前が"ゴミ箱"だったら処理。
		if(e.getInventory().getName().contains("ゴミ箱")){
			//"inv"の中身を消す。
			inv.clear();
			player.sendMessage(ChatColor.RED+"ゴミ箱を焼却しました。");
			player.playSound(player.getLocation(), Sound.DIG_GRAVEL, 1, 5);
		}
	}
	@EventHandler
	//インベントリがクリックされた時発生。
	public void onInventoryClick(InventoryClickEvent e)
	{
		ItemStack item = e.getCurrentItem();
		//"item"がnullだった場合終了。
		if(item == null)
		{
			return;
		}
		//クリックしたインベントリの名前が"ゴミ箱"だったら処理。
		if(e.getInventory().getName().contains("ゴミ箱"))
		{
			//クリックしたアイテムがfireだったら処理。
			if(item.getType().equals(Material.FIRE)){
				e.setCancelled(true);
				inv.clear();
				ItemStack ts = new ItemStack(Material.FIRE);
				ItemMeta testm = ts.getItemMeta();
				testm.setDisplayName("焼却方法");
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.YELLOW+"ゴミ箱を閉じると焼却されます。");
				testm.setLore(lore);
				ts.setItemMeta(testm);
			}
		}
	}
	public boolean onCommand(final CommandSender sender, Command cmd, String label, final String[] args){
		if(cmd.getName().equalsIgnoreCase("senden")){
			final Player p = (Player) sender;
			String name = p.getName();
			ChatColor wh = ChatColor.RESET;
			ChatColor gr = ChatColor.DARK_GREEN;
			String pre = gr+"["+ wh +"宣伝"+ gr +"] ";

			if(args[0].equalsIgnoreCase("buy")){
				//"name"のお金が""宣伝.金額"より多ければ処理。
				if(getConfig().getInt("宣伝.金額") <=econ.getBalance(name)){
					sd.put(p, sd.get(p)+1);
					//"name"のお金から"宣伝.金額"の数だけ取り出す。
					econ.bankWithdraw(name, getConfig().getInt("宣伝.金額"));
					p.sendMessage(pf + "宣伝メッセージを"+ wh +""+ getConfig().getInt("宣伝.金額") +"かまかま"+ gr +"で購入しました！ "+ ChatColor.GRAY +"購入数("+ sd.get(p) +")");
					p.sendMessage(pf + "宣伝回数はﾘログをするとリセットされます。");
					//逆の場合処理。
				} else {
					p.sendMessage(pf + ChatColor.RED +"かまかまが不足しています！");
					return false;
				}
			}

			if(args[0].equalsIgnoreCase("say")){
				if(!(sd.get(p) >=1)){
					p.sendMessage(pre + ChatColor.RED +"宣伝権限を購入していません！");
					return false;
				}
				if(sd.get(p) >=1){
					if(allow.contains(p)){
						Bukkit.broadcastMessage(gr+"["+ wh +"宣伝"+ gr +"] "+ wh +""+ name +""+ gr +"からの宣伝です！");
						Bukkit.broadcastMessage(gr+"["+ wh +"宣伝"+ gr +"] "+ ChatColor.LIGHT_PURPLE +""+ args[1]);
						p.sendMessage(pre + ChatColor.RED +"今から"+ getConfig().getInt("宣伝.クールダウン") +"秒宣伝できません！");
						sd.put(p, sd.get(p)-1);
						allow.remove(p);
						Bukkit.getServer().getScheduler().runTaskLater(this, new Runnable() {
							public void run() {
								allow.add(p);
							}
						}, 20L * getConfig().getInt("宣伝.クールダウン")); 
					} else {
						p.sendMessage(pre + ChatColor.RED +"現在あなたは宣伝できません！");
						return false;
					}
				}
			}
		}
		if(cmd.getName().equalsIgnoreCase("gomibox")){
			Player p = (Player) sender;

			Inventory inv = Bukkit.createInventory(p, 27, ChatColor.BOLD + p.getName() + "のゴミ箱");

			ItemStack ts = new ItemStack(Material.FIRE);
			ItemMeta testm = ts.getItemMeta();
			testm.setDisplayName("焼却方法");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.YELLOW+"ゴミ箱を閉じると焼却されます。");
			testm.setLore(lore);
			ts.setItemMeta(testm);

			inv.setItem(26, ts);
			p.openInventory(inv);
			p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1, 1);
		}
		return false;
	}
}